package com.example.celctofar;

public class TempConverter {


    public double convert(String tempInCelc){
        double tmp = Double.parseDouble(tempInCelc);
        return (tmp*1.8)+32;
    }
}
