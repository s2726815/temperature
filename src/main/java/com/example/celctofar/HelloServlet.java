package com.example.celctofar;

import java.io.*;
import java.text.DecimalFormat;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(name = "helloServlet", value = "/hello-servlet")
public class HelloServlet extends HttpServlet {
    private String message;
    private TempConverter tempConverter;
    public void init() {
        tempConverter = new TempConverter();
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Temperature Converter";
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Temperature in C: " +
                request.getParameter("temp") + "\n" +
                "  <P>Temperature in F: " +
                (new DecimalFormat("0.00").format(tempConverter.convert(request.getParameter("temp")))) +
                "</BODY></HTML>");
    }

    public void destroy() {
    }
}